# r.stream.carve
A GRASS GIS module for carving through man made barriers in stream networks that
often are present in LiDAR based DEMs

# Manual
https://invafish.gitlab.io/r.stream.carve

# Author
Stefan Blumentrath, [Norwegian Institute for Nature Research (NINA)](https://www.nina.no/), Oslo, Norway

Written for the [INVAFISH](https://prosjektbanken.forskningsradet.no/#/project/NFR/243910)
project (RCN MILJ&Oslash;FORSK grant 243910)
