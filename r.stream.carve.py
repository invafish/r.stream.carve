#!/usr/bin/env python

"""
MODULE:    r.stream.carve

AUTHOR(S): Stefan Blumentrath <stefan.blumentrath at nina.no>

PURPOSE:   Carve lines through man made structures in stream networks (e.g. culverts, bridges and similar)

COPYRIGHT: (C) 2019 by Stefan Blumentrath and the GRASS Development Team

This program is free software under the GNU General Public
License (>=v2). Read the file COPYING that comes with GRASS
for details.
"""

#%module
#% description: Carve lines through man made structures in stream networks (e.g. culverts, bridges and similar)
#% keyword: raster
#% keyword: stream
#% keyword: lidar
#% keyword: carve
#% keyword: 3D
#%end

#%option G_OPT_V_INPUT
#% description: Lines to be carved into the digital elevation model
#%end

#%option G_OPT_DB_WHERE
#%end

#%option G_OPT_V_FIELD
#%end

#%option G_OPT_R_ELEV
#% description: Digital elevation model where lines should be carved into
#%end

#%option G_OPT_R_OUTPUT
#% description: Digital elevation model with lines carved into it
#%end

#%option
#% key: memory
#% description: Memory to be used for rasterisation
#% type: integer
#% required: no
#%end

#%option
#% key: method
#% description: Sampling interpolation method
#% options: nearest, bilinear, bicubic
#% answer: bilinear
#% required: no
#%end

"""
ToDo:
- Get raster resolution for interpolation of points
- Write at exit procedure and clean up
- use parallel lines instead of r.neighbors?
- add width option for widening the lines
- Check input for lines
- check if options work
"""

import atexit
import sys
import grass.script as gscript


def cleanup(prefix):
    gscript.run_command('g.remove', type='raster,vector', flags='f',
                        pattern='{}*'.format(prefix))


def main():
    options, flags = gscript.parser()
    input = options['input']
    layer = options['layer']
    where = options['where']
    elevation = options['elevation']
    output = options['output']
    method = options['method']
    memory = options['memory']

    # Create prefix for temporary maps
    tempname = gscript.tempname(9)

    # Check for lines
    # TBD

    # Convert lines to 3D if they ar not 3D yet
    gscript.run_command('v.drape', overwrite=True, verbose=True,
                        input=input, layer=layer, type="line", where=where,
                        output="{}_3D".format(tempname), elevation=elevation,
                        method=method, scale=1.0)

    # Create 3D points along the lines
    # ToDo: use resolution of the terrain model for dmax
    gscript.run_command('v.to.points', flags='it', overwrite=True,
                        input="{}_3D".format(tempname), type='line', dmax=1,
                        output="{}_3D_points".format(tempname))

    # Write point hight to raster (r.in.xyz with min method might be better...)
    gscript.run_command('v.to.rast', overwrite=True, verbose=True,
                        input="{}_3D_points".format(tempname), type='point',
                        output="{}_3D_points".format(tempname), use='z',
                        memory=memory)

    # Widen the footprint of the lines
    gscript.run_command('r.neighbors', verbose=True, overwrite=True,
                        input="{}_3D_points".format(tempname),
                        output="{}_3D_points_mean3".format(tempname),
                        method='average')

    # Produce carved DEM
    expression = '{0}=if(isnull({1}_3D_points_mean3), \
{2}, {1}_3D_points_mean3)'.format(output, tempname, elevation)
    gscript.run_command('r.mapcalc', overwrite=True, verbose=True,
                        expression=expression)

    gscript.raster_history(output, overwrite=True)
    cleanup(tempname)

if __name__ == "__main__":
    sys.exit(main())
